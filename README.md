Ce dépôt fournit les outils de base pour déployer un système de production d’epubs conformes aux recommandations d’accessibilité en ne se basant que sur des outils en ligne de commande dans un terminal shell Bash.

Le manuel d’usage détaillé que l’on peut générer avec cet outil est généré depuis le dépôt [Manuel du Système de Publication Framabook](https://framagit.org/framabook/manuel-du-systeme-de-publication-framabook) ([lien vers la dernière version](https://cloud.framabook.org/s/jpS9Wsn2epPpYyW)).

Pour l’installer, il suffit de se placer dans le répertoire parent de celui qui contient les sources (et nommé ''src'') puis de lancer la commande :

    git clone git@framagit.org:framabook/systeme-publication-framabook.git

Pour pouvoir lancer les commandes de base, il est nécessaire d’avoir installé sur son système :

- pandoc
- zip / unzip
- make
- git

Pour pouvoir générer des pdf, il conviendra d’avoir les outils LaTeX qui comprend xetex/xelatex, voire un éditeur dédié du type TeXMaker pour se faciliter la tâche d’édition. Mais ils ne sont absolument pas utiles pour la génération des epub. Attention, la génération du pdf étant très standardisée, elle ne peut être utile sans retouches manuelles pertinentes au niveau du fichier LaTeX. Sans cela, des défauts d’affichage risquent d’apparaître.

Les autres logiciels nécessaires pour la vérification, accessoires sont :

- epubcheck
- ace (via nodejs, voir [la page d’installation du Consortium Daisy](https://daisy.github.io/ace/getting-started/installation/))

Il suffit ensuite de taper depuis la racine du dépôt la commande :

    make

pour obtenir dans le sous-répertoire 'build' le manuel d’utilisation sous format epub. La source en est bien évidemment dans le sous-répertoire 'src'.

![](schema_makefile.svg)


