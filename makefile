# Nom du fichier final
TITRE      := $(shell grep "^title:" src/informations.yaml | sed -s 's@title: \([^\#]*\).*@\1@' | sed -r 's/ /_/g'| sed -r 's/^(.*)_/\1/g')

# Inclusion conditionnelle de la couverture
COVER      := src/cover.jpg
ifneq ($(wildcard $(COVER)),)  # "src/cover.jpg" s’il est présent, une variable vide sinon
	DRAFT_PANDOC_OPTIONS := --epub-cover-image=$(COVER)
endif

# Liste des fichiers à remplacer
REPLACED_FILES := $(wildcard work/replaced_files/*)

# Liste des fichiers à effacer
DELETED_FILES := $(shell cat work/delete_list.txt)

all:
	$(MAKE) firstpass
	$(MAKE) regexp
	$(MAKE) preparebuild
	$(MAKE) epub

allfromepub:
	$(MAKE) dezip
	$(MAKE) regexp
	$(MAKE) preparebuild
	$(MAKE) epub

epub: build/$(TITRE).epub

all+pdf: all pdf

firstpass: src/informations.yaml src/*.md src/regexp.sed
	@ rm -Rf deflate/*
	@ mkdir --parents tmp
	@ pandoc --resource-path .:src src/*.md -o tmp/draft1.md
	@ echo "\n---" | cat - src/informations.yaml > tmp/informations.yaml
	@ echo "\n---\n" >> tmp/informations.yaml
	@ cat tmp/draft1.md >> tmp/informations.yaml
	@ mv tmp/informations.yaml tmp/draft1.md
	@ pandoc --resource-path .:src tmp/draft1.md $(DRAFT_PANDOC_OPTIONS) -o tmp/draft1.epub
	@ unzip -q tmp/draft1.epub -d deflate
	@ rm tmp/draft1.epub

dezip: src/*.epub
	@ if [ -d "fromepub" ]; then rm -Rf fromepub ; echo "Effacement et recréation du dosier fromepub" ; fi ; mkdir --parents fromepub
	@ unzip -q src/*.epub -d fromepub
	@ if [ -d "deflate" ]; then rm -Rf deflate ; echo "Effacement et recréation du dosier deflate" ; fi ; mkdir --parents deflate
	@ cp -r fromepub/* deflate/
# La section ci-dessous crée les informations de metadata dans le fichier content.opf
# à partir du contenu indiqué dans src/informations.yaml
	@ mkdir --parents tmp
	@ opf=$$(find fromepub/* -name "*.opf");\
	cat $$opf | sed -rz 's/<metadata.*<\/metadata>//g' > tmp/opftemp.opf
	@ mkdir --parents tmp/false
	@ echo "# Dummy title\nDummy text" > tmp/false/draft1.md
	@ echo "\n---" | cat - src/informations.yaml > tmp/false/informations.yaml
	@ echo "\n---\n" >> tmp/false/informations.yaml
	@ cat tmp/false/draft1.md >> tmp/false/informations.yaml
	@ mv tmp/false/informations.yaml tmp/false/draft1.md
	@ pandoc --resource-path .:src tmp/false/draft1.md $(DRAFT_PANDOC_OPTIONS) -o tmp/false/draft1.epub
	@ if [ -d "tmp/false/deflate" ]; then rm -Rf tmp/false/deflate ; echo "Effacement de tmp/false/deflate" ; fi
	@ mkdir --parents tmp/false/deflate
	@ unzip -oq tmp/false/draft1.epub -d tmp/false/deflate
# On écrase l’ancien fichier *.opf extrait de l’epub avec celui généré
# et on place le tout dans le répertoire /deflate à la bonne place pour qu’il soit pris en compte
	@ opfmeta=$$(find tmp/false/deflate/* -name "*.opf");\
	cat $$opfmeta | sed -rz 's/.*(  <metadata.*<\/metadata>\n).*/\1/g' > tmp/opfmeta.opf;\
	opfpath=$$(find fromepub/* -name "*.opf");\
	opfnewpath=$$(echo $$opfpath | sed -r 's/fromepub\//deflate\//g');\
	mkdir --parents $$(dirname $$opfnewpath);\
	packageline=$$(grep -n "package version=" $$opfpath |sed  's/\([0-9]*\).*/\1/');\
	metadataline=$$(( $$packageline + 1));\
	awk "FNR==$$value($$metadataline){system(\"cat tmp/opfmeta.opf\")} 1" tmp/opftemp.opf > $$opfnewpath
# Il faut aussi remplacer l’ancienne couverture par la nouvelle si elle existe
ifneq ($(wildcard $(COVER)),)
	@ echo "Changement de la couverture"
	@ opfwork=$$(find fromepub/* -name "*.opf");\
	coverid=$$(grep "<meta name=\"cover\" content=\".*\".*/>" $$opfwork | sed -rz 's/.*<meta name="cover" content="(.*)" \/>/\1/g');\
	echo "coverid :" $$coverid;\
	fullcoverfilepath=$$(grep ".*<item.*id=\"$$coverid\" href=\".*\" media-type=\".*\"/>" $$opfwork | sed -rz 's/.*<item.*href=\"(.*)\".*media-type.*/\1/g');\
	echo "fullcoverfilepath :" $$fullcoverfilepath;\
	coverfile=$$(echo "$$(basename "$${fullcoverfilepath}")");\
	echo "coverfile :" $$coverfile;\
	covermetadata=$$(grep ".*<item.*id=\"$$coverid\" href=\".*\" media-type=\".*\"/>" $$opfwork | sed -rz 's/media-type=\".*\"/media-type=\"image\/jpeg\"/g');\
	echo "covermetadata :" $$covermetadata;\
	coverfilepath=$$(find deflate/* -name $$coverfile | sed -r 's/fromepub\//deflate\//g');\
	echo "coverfilepath :" $$coverfilepath;\
	newdirectory=$$(echo "$$(dirname "$${coverfilepath}")");\
	echo "newdirectory :" $$newdirectory;\
	mkdir --parents $$newdirectory;\
	cp $(COVER) $$newdirectory;\
	newcover=$$(echo "$$(basename "$(COVER)")");\
	echo "newcover :"$$newcover;\
	if [ ! -f "src/regexp.sed" ]; then touch src/regexp.sed; echo "Ajout d’un fichier src/regexp.sed temporaire" ;\
	else echo "\n" >> src/regexp.sed;\
	fi ;\
	echo "s\t\([/\"]\)$$coverfile\t\\\\1$$newcover\t" >> src/regexp.sed;\
	echo "s\thref=\"\(.*\.jpg\)\".*media-type=\"image/png\"\thref=\"\\\\1\" media-type=\"image\/jpeg\"\t">> src/regexp.sed
else
	@echo "Pas de couverture"
endif

build/$(TITRE).epub: preparebuild
	@ mkdir --parents build
	@ cd tmp/epub && zip -qrX ../../build/'$(TITRE)'.epub *

preparebuild: regexp
	@ mkdir --parents tmp/epub
	@ cp -r deflate/* tmp/epub
ifneq ($(REPLACED_FILES),)
	@echo "Fichiers à écraser"
	@ cp -r work/replaced_files/* tmp/epub
endif
	@ for f in $(DELETED_FILES) ; do rm -f "tmp/epub/$${f}" ; done

regexp: src/regexp.sed
# ajouter «-name "*.txt" -o -name "*.md"» si on veut préciser les extensions des fichiers à modifier
	@ find deflate/* -type f -exec sed -f "$$(readlink -f -- "src/regexp.sed")" --in-place {} \;

validate: build/$(TITRE).epub
	@ - epubcheck -j build/epubcheck_report.json build/monepub_final.epub # Le - est nécessaire en début de commande make pour que make ne s’arrête pas si il reçoit une erreur de cette commande
	@ ace --force --outdir build/ace_reports build/$(TITRE).epub

latex: src/*.md templates/*
	@ mkdir --parents tex
	@ mkdir --parents tmp
	@ echo "\n---" | cat - src/informations.yaml > tmp/informations_tex.yaml
	@ echo "\n---\n" >> tmp/informations_tex.yaml
	@ pandoc --resource-path .:src --extract-media=tex/ -s src/*.md tmp/informations_tex.yaml --template templates/book.latex -o tex/$(TITRE).tex

pdf: latex
	@ mkdir --parents build
	@ - xelatex -output-directory tex tex/$(TITRE).tex # Le - est nécessaire en début de commande make pour que make ne s’arrête pas si il reçoit une erreur de cette commande
	@ cp tex/$(TITRE).pdf build/$(TITRE).pdf

clean:
	@ rm -Rf deflate
	@ rm -Rf fromepub
	@ rm -Rf build
	@ rm -Rf tmp
